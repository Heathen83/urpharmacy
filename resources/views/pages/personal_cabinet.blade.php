@include('layouts.header', ['title' => 'Особистий кабінет'])
@include('layouts.nav')
<div class="content personal__cabinet">
    <div class="container">
        <div class="content__cabinet__breadcrumb">
            <ul>
                <li><a href="{{ route('pages.index') }}">Головна</a></li>
                <li><span>&nbsp;/&nbsp;</span></li>
                <li><span>Особистий кабiнет</span></li>
            </ul>
        </div>
        <div id="cabinetNav" class="content__cabinet"><h5>Особистий кабінет</h5>
            <ul>
                <li><a href="#tabs-1">Профіль</a></li>
                <li><a href="#tabs-2">Мої замовлення</a></li>
                <li><a href="#tabs-3">Обрані товари</a></li>
                <li>
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        Вийти з кабінету
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
            <div id="tabs-1" class="profiles">
                <div class="custumer__contacts"><h6>Контактнi данi</h6>
                    <div class="contacts">
                        <div class="key">
                            <p>Телефон</p>
                            <p>ПІБ</p>
                        </div>
                        <div class="value"><p id="tel">{{ $user->phone }}</p>
                            <p id="firstName">{{ $user->name }}</p>
                            <form id="contactsForm" action="{{ route('user.change.general') }}" method="POST">
                                @csrf
                                <p><input type="tel" value="{{ $user->phone }}" name="tel" class="js-phone"></p>
                                <p><input type="text" value="{{ $user->name }}" name="firstName"></p>
                                <button id="contactsSave" form="contactsForm" type="submit" class="contactsSave">
                                    Зберегти
                                </button>
                            </form>
                        </div>
                    </div>
                    <button id="contactsEdit" type="button">Редагувати</button>
                </div>
                <div class="custumer__email"><h6>Електрона пошта</h6>
                    <div class="contacts">
                        <div class="value"><p id="email">{{ $user->email }}</p>
                            <form id="emailForm" action="{{ route('user.change.email') }}" method="post">
                                @csrf
                                <p><input type="text" name="email"></p>
                                <button id="emailSave" type="submit" class="emailSave">Зберегти</button>
                            </form>
                        </div>
                    </div>
                    <button id="emailEdit" type="button">Редагувати</button>
                </div>
                <div class="custumer__password"><h6>Пароль</h6>
                    <div class="contacts">
                        <div class="value"><p id="password">************</p>
                            <form id="passwordForm" action="{{ route('user.change.password') }}" method="post">
                                @csrf
                                <p>Новий пароль</p><input id="pswd" type="password" name="newPassword">
                                <p>Повторiть пароль</p><input id="confirmPswd" type="password" name="confirmPassword">
                                <button id="passwordSave" type="submit" class="passwordSave">Зберегти</button>
                            </form>
                        </div>
                    </div>
                    <button id="passwordEdit" type="button">Редагувати</button>
                </div>
            </div>
            <div id="tabs-2" class="myOrders"><h6>Мої замовлення</h6>
                @if($user->orders)
                <ul>
                    @foreach($user->orders as $order)
                        <li><p>від {{ $order->created_at }}</p><span>{{ $order->total  }} грн</span></li>
                    @endforeach
                </ul>
                @endif
            </div>
{{--            <div style="display: none">
                <div id="orderInfo" class="orderInfo"><i id="infoClose" class="icon-cancel"></i>
                    <h5>Замовлення від {{ $order->created_at }}</h5>
                    <ul>
                        <li>
                            <div><p class="value">Тивортин аспартат р-р оральный фл. 100мл</p>
                                <div class="quantitu"><span class="minus">-</span><input type="text" value="1"><span
                                            class="plus">+</span></div>
                                <p class="price">181.76<span>&nbsp;грн</span></p><span class="delete"><i
                                            class="icon-cancel"></i></span></div>
                        </li>
                    </ul>
                    <div id="total" class="total"><h6>Загальна сума</h6>
                        <p>10481.76<span>&nbsp;грн</span></p></div>
                    <button id="repeatOrder" type="submit">Повторити замовлення</button>
                </div>
            </div>--}}
            <div id="tabs-3" class="featuredProducts">
                @foreach($user->favorites as $product)
                    <div>
                        @include('layouts.product_element', ['product' => $product->drug])
                    </div>
                @endforeach
            </div>
        </div>
        <div class="content__howToMakeOrder">
            <div><p class="icons"><i class="icon-right-arrow-thin arrow"></i><i class="icon-shopping-cart cart"></i></p>
                <p class="learnMore"><span id="orderPopup">Дізнатись більше</span></p>
                <p class="info"><span>Як зробити замовлення</span><br><span>Пошук та вибір товару</span></p></div>
            <div><p class="icons"><i class="icon-right-arrow-thin arrow"></i><i class="icon-box box"></i></p>
                <p class="learnMore"><span id="deliveryPopup">Дізнатись більше</span></p>
                <p class="info"><span>Доставка і оплата</span><br><span>Способи, строки, умови</span></p></div>
            <div><p class="icons"><i class="icon-right-arrow-thin arrow"></i><i class="icon-shipped truck"></i></p>
                <p class="learnMore"><span id="underOrderPopup">Дізнатись більше</span></p>
                <p class="info"><span>Товари під замовлення</span><br><span>Безкоштовна доставка</span></p></div>
        </div>
    </div>
</div>
@include('layouts.footer')