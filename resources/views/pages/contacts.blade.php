@include('layouts.header', ['title' => 'Контакти'])
@include('layouts.nav')
<div class="content contacts">
    <div class="container">
        <div class="content__contacts__breadcrumb">
            <ul>
                <li><a href="{{ route('pages.index') }}">Головна</a></li>
                <li><span>&nbsp;/&nbsp;</span></li>
                <li><span>Контакти</span></li>
            </ul>
        </div>
        <div class="content__contacts"><h5>Контакти</h5>
            <ul>
                <li><h6>Адреса</h6>
                    <p>м. Хмельницький<br>вул. Будівельників, 3</p></li>
                <li><h6>Телефон адміністрації</h6>
                    <p>тел. <a href="tel:+380639562389">+38 (063) 956-23-89</a>
                    <br>тел. <a href="tel:+380639562389">+38 (063) 956-23-89</a></p>
                </li>
                <li><h6>Телефон для замовлення ліків</h6>
                    <p>тел. <a href="tel:+380639562389">+38 (063) 956-23-89</a><br>тел. <a href="tel:+380639562389">+38 (063) 956-23-89</a></p></li>
            </ul>
        </div>
    </div>
</div>
@include('layouts.footer')