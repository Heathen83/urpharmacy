@include('layouts.header', ['title' => 'Каталог'])
@include('layouts.nav')
<div class="content catalog">
    <div class="container">
        @include('layouts.aside_nav')
        <div class="content__catalog__breadcrumb">
            <ul>
                <li><a href="{{route('pages.index')}}">Головна</a></li>
                <li><span>&nbsp;/&nbsp;</span></li>
                <li><span>{{ $category ?? 'Всі товари' }}</span></li>
            </ul>
        </div>
        <div class="content__catalog"><h5>{{ $category ?? 'Всі товари' }}</h5>
            <div class="catalog__options">
            <form method="GET">
                <div class="catalog__options__goods"><p>Товарiв:&nbsp;&nbsp;<span>{{ $products->total() }}</span></p>
                    <select name="formType">
                        <option value="default">Всі форми випуску</option>
                        @isset($formTypes)
                            @foreach($formTypes as $index => $type)
                                <option value="{{ $index }}" {{ $formType == $index ? 'selected' : '' }}>{{ $type }}</option>
                            @endforeach
                        @endisset
                    </select>
                    <select name="makerType">
                        <option value="default">Всі виробники</option>
                        @isset($makerTypes)
                            @foreach($makerTypes as $index => $type)
                                <option value="{{ $index }}" {{ ( $makerType == $index ) && ($makerType != '') ? 'selected' : '' }}>{{ $type }}</option>
                            @endforeach
                        @endisset
                    </select>
                    <select name="rating">
                        <option value="default" selected>Популярність</option>
                        <option value="price-cheap" {{ $rating === 'price-cheap' ? 'selected' : '' }}>Цiна (Дешевше-Дорожче)</option>
                        <option value="price-costly" {{ $rating === 'price-costly' ? 'selected' : '' }}>Цiна (Дорожче-Дешевше)</option>
                        <option value="availability" {{ $rating === 'availability' ? 'selected' : '' }}>Наявнiсть</option>
                    </select>
                    <button type="submit" class="button-custom" style="margin-left: 1em;">Пошук</button>
                </div>
            </form>
            </div>
            <div class="catalog__goods">
                @if ($products)

                    @if ($products->total() == 0)
                        <p class="text-style">Товари не знайдено.</p>
                    @else
                        @foreach($products as $product)
                            <div class="product">
                                @include('layouts.product_element', ['product' => $product])
                            </div>
                        @endforeach
                    @endif

                @else
                    <p style="padding: 1em;margin: 0 auto;font-size: 1.5em">Товари не знайдено</p>
                @endif
            </div>
            @if($products->currentPage() != $products->lastPage())
            <div class="load_more">
                <button type="button" class="button-custom" id="load_more" data-current="{{ $products->currentPage() }}"><i class="imf icon-spinner11"></i>  Показати більше товарів</button>
            </div>
            @endif
            <div style="padding-bottom: 1em" class="catalog__paginator">

                @include('layouts.paginator', ['products' => $products])

            </div>
        </div>
        <div class="content__howToMakeOrder">
            <div><p class="icons"><i class="icon-right-arrow-thin arrow"></i><i class="icon-shopping-cart cart"></i></p>
                <p class="learnMore"><span id="orderPopup">Дізнатись більше</span></p>
                <p class="info"><span>Як зробити замовлення</span><br><span>Пошук та вибір товару</span></p></div>
            <div><p class="icons"><i class="icon-right-arrow-thin arrow"></i><i class="icon-box box"></i></p>
                <p class="learnMore"><span id="deliveryPopup">Дізнатись більше</span></p>
                <p class="info"><span>Доставка і оплата</span><br><span>Способи, строки, умови</span></p></div>
            <div><p class="icons"><i class="icon-right-arrow-thin arrow"></i><i class="icon-shipped truck"></i></p>
                <p class="learnMore"><span id="underOrderPopup">Дізнатись більше</span></p>
                <p class="info"><span>Товари під замовлення</span><br><span>Безкоштовна доставка</span></p></div>
        </div>
    </div>
</div>
<?php
//    $url = explode("/",url()->current());
//    array_splice( $url, 3, 0, 'ajax' );
//    $url = implode("/",$url);
        $url = url()->current();
?>
@include('layouts.footer')

<script>
    $('#load_more').click(function() {
        var current =  $(this).data("current");
        current+=1;
        $(this).data("current",current);

        if(current == {!! $products->lastPage() !!})
            $(this).hide();

        $.ajax({
            url: '{!! $url !!}?page='+(current)+"&load=t",

            type: 'GET',
            success: function (data) {
                $('.catalog__goods').append(data.html);
//                console.log(data);

                $(".catalog__paginator").html(data.paginator);
            }
        });

    });
</script>