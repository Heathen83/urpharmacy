@include('layouts.header', ['title' => 'Класифікація'])
@include('layouts.nav')
<div class="content medical__devices">
    <div class="container">
        @include('layouts.aside_nav')
        <div class="content__catalog__breadcrumb">
            <ul>
                <li><a href="{{ route('pages.index') }}">Головна</a></li>
                <li><span>&nbsp;/&nbsp;</span></li>
                <li><span>Класифікація АТС</span></li>
            </ul>
        </div>
        <div class="content__catalog">
            <div id="medDev__accordeon" class="content__catalog__medicalDevices"><h5>Класифікація АТС</h5>
                @foreach($categories as $category)
                @if($category)
                <div class="madicalDevices_A">

                    <h6>
                        <i class="icon-plus-symbol-in-a-rounded-black-square"></i>
                        <a href="{{ route('sub-classification.show', $category->id) }}">&nbsp;&nbsp;{{ $category->abbreviation }} - {{ $category->title }} </a>
                    </h6>

                    <ul>
                        @if($category->subcategories)
                            @foreach($category->subcategories as $subcategory)
                                <li style="padding: 1em;">
                                    <a href="{{ route('sub-classification.show', $subcategory->id) }}" class="sub_classification">
                                        <i class="icon-plus-symbol-in-a-rounded-black-square"></i>
                                        &nbsp;&nbsp;{{ $subcategory->abbreviation }} - {{ $subcategory->title }}
                                    </a>
                                </li>
                            @endforeach
                        @endif
                        {{--@if ($category->drugs)--}}
                            {{--@foreach($category->drugs as $drug)--}}
                                {{--<li style="padding: 1em;">--}}
                                    {{--<a href="{{ route('product.show', $drug->code) }}" style="text-decoration: none;color:black;">--}}
                                        {{--<i class="icon-medicines"></i>--}}
                                        {{--&nbsp;&nbsp;{{ $drug->title }}--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            {{--@endforeach--}}
                        {{--@endif--}}
                    </ul>

                </div>
                @endif
                @endforeach
            </div>

        </div>
        <div class="content__catalog" style="grid-area: catalog1;">

            <div class="catalog__goods">
                @if ($products)
                    @foreach($products as $product)

                        <div class="product">
                            @include('layouts.product_element', ['product' => $product])
                        </div>
                    @endforeach
                @else
                    <p style="padding: 1em;margin: 0 auto;font-size: 1.5em">Товари не знайдено</p>
                @endif
            </div>


            @if($products->currentPage() != $products->lastPage())
                <div class="load_more" style="width: 100%;">
                    <button type="button" class="button-custom" id="load_more" data-current="{{ $products->currentPage() }}"><i class="imf icon-spinner11"></i>  Показати більше товарів</button>
                </div>
            @endif
        </div>

        <div class="content__howToMakeOrder">
            <div><p class="icons"><i class="icon-right-arrow-thin arrow"></i><i class="icon-shopping-cart cart"></i></p>
                <p class="learnMore"><span id="orderPopup">Дізнатись більше</span></p>
                <p class="info"><span>Як зробити замовлення</span><br><span>Пошук та вибір товару</span></p></div>
            <div><p class="icons"><i class="icon-right-arrow-thin arrow"></i><i class="icon-box box"></i></p>
                <p class="learnMore"><span id="deliveryPopup">Дізнатись більше</span></p>
                <p class="info"><span>Доставка і оплата</span><br><span>Способи, строки, умови</span></p></div>
            <div><p class="icons"><i class="icon-right-arrow-thin arrow"></i><i class="icon-shipped truck"></i></p>
                <p class="learnMore"><span id="underOrderPopup">Дізнатись більше</span></p>
                <p class="info"><span>Товари під замовлення</span><br><span>Безкоштовна доставка</span></p></div>
        </div>
    </div>
</div>


<?php
    $url = url()->current();
?>
@include('layouts.footer')

<script>
    $('#load_more').click(function() {
        var current =  $(this).data("current");
        current+=1;
        $(this).data("current",current);

        if(current == {!! $products->lastPage() !!})
            $(this).hide();

        $.ajax({
            url: '{!! $url !!}?page='+(current)+"&load=t",

            type: 'GET',
            success: function (data) {
                $('.catalog__goods').append(data.html);
            }
        });

    });
</script>