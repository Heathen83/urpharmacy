@include('layouts.header', ['title' => 'Корзина'])
@include('layouts.nav')
<div class="content busket">
    <div class="container">
        <div class="content__busket__breadcrumb">
            <ul>
                <li><a href="{{ route('pages.index') }}">Головна</a></li>
                <li><span>&nbsp;/&nbsp;</span></li>
                <li><span>Корзина</span></li>
            </ul>
        </div>
        <div class="content__busket"><h5>Корзина</h5>
            <form id="makeOrder" action="{{ route('order.make') }}" method="POST">
            <div class="isProcess" data-status="false">
                <div class="content">
                    @csrf
                    <ul>
                        @foreach ($products as $item)
                        <li>
                            <div>
                            <div class="product-wrapper">
                                @if(count($item->drug->images))
                                    <img src="{{asset($item->drug->images[0]->path)}}" alt="">
                                @else
                                    <img src="{{'/public/images/404.jpg'}}" alt="">
                                @endif
                            </div>
                            <a class="value" href="{{ route('product.show', $item->drug->code) }}">{{ $item->drug->title }}. {{ $item->title }}</a>
                                <input type="hidden" name="items[{{ $item->id }}][code]" value="{{ $item->rowId }}">
                                <div class="quantitu">
                                    <span class="minus">-</span>
                                    <input readonly type="text" class="changeQty" data-id="{{ $item->rowId }}" name="items[{{ $item->id }}][quantity]" value="{{ $item->cartQuantity }}">
                                    <span class="plus">+</span>
                                </div>
                                <p class="price">{{ $item->price }}<span>&nbsp;грн</span></p>
                                <span class="delete deleteItem" data-id="{{ $item->rowId }}"><i class="icon-cancel"></i></span>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                    <div id="total" class="total">
                        <h6>Загальна сума</h6>
                        <p id="totalCart">{{ $total }}</p><span>&nbsp;грн</span>
                    </div>
                    <div><a href="{{ route('cart.clear') }}" style="text-decoration: none;color: gray">Очистити корзину</a></div>
                </div>
            </div>

            <div class="costumerInfo">
                <p><input value="{{ old('name') }}" type="text" style="margin-bottom: 10px" name="name" placeholder="Ім’я" required>
                    <i class="icon-asterisk asterisk"></i>

                    @if ($errors->has('name'))
                    <span class="invalid-feedback">
                        <p><strong>{{ $errors->first('name') }}</strong></p>
                    </span>
                    @endif
                </p>
                <p>
                    <input value="{{ old('tel') }}" type="tel" name="tel" placeholder="Телефон" class="js-phone" required>
                    <i class="icon-asterisk asterisk"></i>
                    <br>
                    @if ($errors->has('tel'))
                    <span class="invalid-feedback">
                        <p><strong>{{ $errors->first('tel') }}</strong></p>
                    </span>
                    @endif
                </p>
                <textarea name='description' placeholder="Додаткові побажання">{{ old('description') }}</textarea>
                @if ($errors->has('description'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
                <br>
                <button type="submit">Купити</button>

                <div style="display: none">
                    <div id="thanksOrder"><i id="thanksOrderClose" class="icon-cancel"></i>
                        <p>Дякуємо за Ваше замовлення!</p>
                        <button form="makeOrder" type="button">OK</button>
                    </div>
                </div>
            </div>
            </form>
        </div>
        <div class="content__howToMakeOrder">
            <div><p class="icons"><i class="icon-right-arrow-thin arrow"></i><i class="icon-shopping-cart cart"></i></p>
                <p class="learnMore"><span id="orderPopup">Дізнатись більше</span></p>
                <p class="info"><span>Як зробити замовлення</span><br><span>Пошук та вибір товару</span></p></div>
            <div><p class="icons"><i class="icon-right-arrow-thin arrow"></i><i class="icon-box box"></i></p>
                <p class="learnMore"><span id="deliveryPopup">Дізнатись більше</span></p>
                <p class="info"><span>Доставка і оплата</span><br><span>Способи, строки, умови</span></p></div>
            <div><p class="icons"><i class="icon-right-arrow-thin arrow"></i><i class="icon-shipped truck"></i></p>
                <p class="learnMore"><span id="underOrderPopup">Дізнатись більше</span></p>
                <p class="info"><span>Товари під замовлення</span><br><span>Безкоштовна доставка</span></p></div>
        </div>
    </div>
</div>
@include('layouts.footer')
<script src="{{ asset('js/cart.js') }}"></script>
</body>