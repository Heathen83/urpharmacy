<li>
    <a href="{{route('category.show', [$category->id])}}">{{ $category->title }}</a>

    @if(count($category->subcategories)>0)
        <div class="more"><i class="icon-chevron-right"></i></div>
        <div class="mobil-wrap">
        <ul class="">
            @foreach($category->subcategories as $d)
                @include('layouts.menu_nav_item', ['category' => $d])
            @endforeach
        </ul>
        </div>
    @endif
</li>
