<?php $count = (\Illuminate\Support\Facades\Session::has('quantity')) ? Session::get('quantity') : 0; ?>
<div id="fixedNav" class="header__fixedNav hidden">
    <ul>
        <li id="catalogDrop"><a href="{{ route('pages.page', 'catalog') }}" class="desktop"><i class="icon-list"></i>Каталог товарів</a><a
                    href="#" class="mobile"><i id="mobileDropButton" class="icon-list"></i></a></li>
        <li>
            <div class="desktop">
                <input type="text" placeholder="Пошук препаратів" class="formSearchValue">
                <i class="icon-magnifier submitForm"></i></button>
            </div>
            <div class="mobile"><i class="icon-magnifier"></i>
                <input type="text" placeholder="Пошук препаратів" class="formSearchValue">
                <i class="icon-left-arrow-thin back submitForm"></i>
            </div>
        </li>
        <li><a href="tel:+380639562389" class="desktop"><i class="icon-auricular-phone-symbol-in-a-circle"></i>&nbsp;&nbsp;0639562389</a><a
                    href="tel:+380639562389" class="mobile"><i class="icon-auricular-phone-symbol-in-a-circle"></i></a>
        </li>
        <li><a href="viber://chat?number=+380639562389"><i class="icon-viber_no_bg"></i></a></li>
        <li><a href="{{ route('cart.show') }}" class="desktop"><i class="icon-shopping-cart"></i>@if($count>0 && !request()->routeIs('cart.show'))<span class="count">{{$count}}</span>@endif&nbsp;&nbsp;Корзина</a><a
                    href="{{ route('cart.show') }}" class="mobile"><i class="icon-shopping-cart"></i>@if($count>0 && !request()->routeIs('cart.show'))<span class="count">{{$count}}</span>@endif</a></li>
    </ul>
</div>
@if (isset($pageHeader) && $pageHeader)
<div id="slideshow" class="slider slider-header">
    <img src="{{ asset('images/main_slider.jpg') }}" alt="">
</div>
<div class="header__main">
@endif
    <div class="header">
        <form method="GET" action="{{ route('product.search', '') }}" data-url="{{ route('product.search', '') }}" id="searchForm"></form>
        <div class="container">
            <div class="header__headline"><p>Лікарські засоби і вироби медичного призначення. Лише сертифікована
                    продукція в Хмельницькому і по всій Україні.</p></div>
            <div class="header__logo"><a href="{{ route('pages.index') }}"></a><span></span></div>
            <div class="header__mainSearch">
                <div class="search">
                    <input type="text" placeholder="Пошук препаратів" class="formSearchValue">
                    <i class="icon-magnifier submitForm"></i>
                </div>
                <div class="numbers"><i class="icon-auricular-phone-symbol-in-a-circle"></i>
                    <ul>
                        <li><i class="icon-auricular-phone-symbol-in-a-circle"></i></li>
                        <li><a href="tel:+380639562389">+38 (063) 956-23-89</a></li>
                    </ul>
                </div>
                <div class="workTime"><i class="icon-passage-of-time"></i>
                    <ul>
                        <li><span>Пн-Пт: 09:00-21:00</span></li>
                        <li><span>Сб-Нд: 11:00-24:00</span></li>
                    </ul>
                </div>
            </div>
            <div class="header__cabinet">
                <ul>
                    <li><a id="authorizationPopup{{Auth::user()? 'Authed' : ''}}" href="{{ Auth::user()? route('cabinet.show') : '#' }}"><span><i class="icon-login-square-arrow-button-outline"></i></span><span>Особистий
кабінет</span></a></li>
                    <li><a id="busket" href="{{ route('cart.show') }}"><span><i
                                        class="icon-shopping-cart"></i>@if($count>0 && !request()->routeIs('cart.show'))<span class="count">{{$count}}</span>@endif</span><span>Корзина</span></a></li>
                    <li><a href="viber://chat?number=+380639562389"><span><i class="icon-viber"></i></span><span>Консультація у Viber</span></a>
                    </li>
                </ul>
            </div>
            <div class="header__nav-panel"><a id="mobileDrop" href="#" class="mobileDrop"><i
                            class="icon-list"></i><span>&nbsp;&nbsp;Каталог товарiв</span></a>
                <div class="header__nav-panel__major">
                    <ul>
                        <li><a href="{{ route('pages.page', 'classification') }}" @if(Request::segment(1) === 'classification') class="active_button" @endif>Класифікація АТС</a></li>
                        <li><a href="{{ route('pages.page', 'about_us') }}" @if(Request::segment(1) === 'about_us') class="active_button" @endif>Про нас</a></li>
                        <li><a href="{{ route('pages.page', 'payment_and_delivery') }}" @if(Request::segment(1) === 'payment_and_delivery') class="active_button" @endif>Оплата і доставка</a></li>
                        <li><a href="{{ route('pages.page', 'discount') }}" @if(Request::segment(1) === 'discount') class="active_button" @endif>Акції</a></li>
                        <li><a href="{{ route('pages.page', 'certificates') }}" @if(Request::segment(1) === 'certificates') class="active_button" @endif>Гарантія</a></li>
                        <li><a href="{{ route('pages.page', 'contacts') }}" @if(Request::segment(1) === 'contacts') class="active_button" @endif>Контакти</a></li>
                    </ul>
                </div>
                <div class="header__nav-panel__goods">
                    <ul>
                        @if (isset($category_types) && $category_types)
                            @foreach ($category_types as $type)
                                <li id="category-{{$type->id}}" class="category" data-url="{{$type->url}}"><span @if(Request::segment(1) === '{{$type->url}}') class="active_button" @endif><i class="{{$type->icon}}"></i>&nbsp;&nbsp;{{$type->name}}</span>
                                    @if (isset($categories) && $categories && collect($categories)->where('type', $type->id)->count() > 0)
                                    <div class="m-wrap">
                                        <ul id="category-{{$type->id}}-drop" class="dropMenu medicalDevices category-drop">
                                            @foreach ($categories as $category)
                                                @if($category->type === $type->id)

                                                    @include('layouts.nav_item', ['category' => $category])

                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div style="display: none">
        <div class="header__nav-panel__mobile">
            <div class="header__nav-panel__goodsMobile">
                <ul>
                    <li>
                        <a href="{{ route('pages.index') }}"><img src="/images/logo.png" alt=""></a></li>
                    <li>
                    <li>
                        <a href="http://urpharmacy/catalog"><i class="icon-list"></i>&nbsp;&nbsp;Каталог товарів</a>
                    </li>
                    @if (isset($category_types) && $category_types)
                        @foreach ($category_types as $type)
                            <li>
                                <a href="{{ route('pages.page', $type->url) }}"><i class="{{$type->icon}}"></i>&nbsp;&nbsp;{{$type->name}}</a>
                                @if (isset($categories) && $categories && collect($categories)->where('type', $type->id)->count() > 0)
                                    <div class="more"><i class="icon-chevron-right"></i></div>
                                    <div class="mobil-wrap">
                                        <ul id="category-{{$type->id}}-drop" class="dropMenu medicalDevices category-drop">
                                            @foreach ($categories as $category)
                                                @if($category->type === $type->id)

                                                    @include('layouts.menu_nav_item', ['category' => $category])

                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
            <div class="header__nav-panel__majorMobile">
                <ul>
                    <li><a href="{{ route('pages.page', 'classification') }}"><i></i>Класифікація АТС</a></li>
                    <li><a href="{{ route('pages.page', 'about_us') }}"><i></i>Про нас</a></li>
                    <li><a href="{{ route('pages.index') }}"><i></i>Оплата і доставка</a></li>
                    <li><a href="{{ route('pages.page', 'discount') }}"><i></i>Акції</a></li>
                    <li><a href="{{ route('pages.page', 'certificates') }}"><i></i>Гарантія</a></li>
                    <li><a href="{{ route('pages.page', 'contacts') }}"><i></i>Контакти</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

{{--@include('layouts.nav_item')--}}
