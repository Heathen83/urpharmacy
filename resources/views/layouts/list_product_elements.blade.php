@foreach($products as $product)
    <div class="product">
        @include('layouts.product_element', ['product' => $product])
    </div>
@endforeach