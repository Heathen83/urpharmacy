@if (isset($product) && $product)
    <a href="{{ route('product.show', $product->drug->code) }}">
        <div class="img">
            <img src="/{{$product->drug->images[0]->path ?? 'images/mstile_150x150.png'}}" alt="">
        </div>
        <div class="product-titles" style="height: 76px; border: none; margin: 0; width: auto;">
            <p style="padding-bottom: 2px; margin-top: 5px; max-height: 42px;">{{ mb_substr($product->drug->title, 0, 40, 'UTF-8') }}@if(mb_strlen($product->drug->title)>45)...@endif</p>
            <p style="padding-top: 0; max-height: 12px;">{{ mb_substr($product->title, 0 , 25, 'UTF-8') }}</p>
        </div>
        @if ($product->quantity)
        <p class="price" {{ $product->discount_price ? 'style=color:red;' : '' }}>{{ $product->discount_price ?? $product->price }}<span>грн</span></p>
        @else
        <p class="price">Немає в наявності</p>
        @endif
    </a>
    <div class="hover">
        <a href="{{ route('product.show', $product->drug->code) }}" class="link" title="{{$product->drug->title}} {{$product->title}}"></a>
        @if ($product->quantity)
        <form action="{{ route('cart.add') }}" method="POST">
            @csrf
            <input type="hidden" name="code" value="{{ $product->id }}">
            <button type="submit" title="Додати в корзину" class="addBusket">
                <i class="icon-shopping-cart">
                    <span style="font: 1em 'PTSans-Regular';vertical-align: text-bottom;"> В корзину</span>
                </i>
            </button>
        </form>
        @endif
        @if ($product->isFavorite())
        <form action="{{ route('favorite.remove') }}" method="POST">
            @csrf
            <input type="hidden" name="code" value="{{ $product->id }}">
            <button type="submit" title="Видалити з обраного" data-active=true class="addFavorite"><i class="icon-heart"></i></button>
        </form>
        @else
        <form action="{{ route('favorite.add') }}" method="POST">
            @csrf
            <input type="hidden" name="code" value="{{ $product->id }}">
            <button type="submit" title="Додати в обране" data-active=false class="addFavorite"><i class="icon-heart"></i></button>
        </form>
        @endif
    </div>

@endif