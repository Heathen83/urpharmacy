@if (isset($drug) && $drug)
    <a href="{{ route('product.show', $drug->code) }}">
        <img src="{{$drug->images[0]->path ?? '/images/mstile_150x150.png'}}" alt="">
        <p style="text-align: center">{{ $drug->title }}</p>
    </a>
    <div class="hover">
        <a href="{{ route('product.show', $drug->code) }}" class="link"></a>
    </div>
@endif