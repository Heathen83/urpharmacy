<ul class="desktop">
    <li><a href="{{ $products->previousPageUrl() }}"><i class="icon-left-arrow"></i></a></li>
    @if($products->lastPage() < 7)
        @for ($i=1; $i <= $products->lastPage(); $i++)
            <li><a href="{{ $products->url($i) }}"
                   class="{{ $products->currentPage() === $i ? 'active' : '' }}"> {{ $i }} </a></li>
        @endfor
    @else

        @for ($i=1; $i<=2; $i++)
            <li><a href="{{ $products->url($i) }}"
                   class="{{ $products->currentPage() === $i ? 'active' : '' }}"> {{ $i }} </a></li>
        @endfor

        <li><a>...</a></li>

        @if(($products->currentPage() > 2) and ($products->currentPage() < $products->lastPage()-1))
            @if($products->currentPage() > 3)
                <li><a href="{{ $products->url($products->currentPage()-1) }}"> {{ $products->currentPage()-1 }} </a></li>
            @endif

            <li><a href="{{ $products->url($products->currentPage()) }}"
                  class="active"> {{ $products->currentPage() }} </a></li>

            @if($products->currentPage() < $products->lastPage()-2)
               <li><a href="{{ $products->url($products->currentPage()+1) }}"> {{ $products->currentPage()+1 }} </a></li>
            @endif

            <li><a>...</a></li>
        @endif


@for ($i=$products->lastPage()-1; $i<=$products->lastPage(); $i++)
   <li><a href="{{ $products->url($i) }}"
          class="{{ $products->currentPage() === $i ? 'active' : '' }}"> {{ $i }} </a></li>
@endfor

@endif

<li><a href="{{ $products->nextPageUrl() }}"><i class="icon-right-arrow"></i></a></li>
</ul>

<ul class="mobile">
<li><a href="{{ $products->previousPageUrl() }}"><i class="icon-left-arrow"></i></a></li>
@for ($i=1; $i<=4; $i++)
<li><a href="{{ $products->url($i) }}"
      class="{{ $products->currentPage() === $i ? 'active' : '' }}"> {{ $i }}</a></li>
@endfor
<li><a href="{{ $products->nextPageUrl() }}"><i class="icon-right-arrow"></i></a></li>
</ul>