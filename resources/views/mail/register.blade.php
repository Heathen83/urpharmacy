@include('layouts.header', ['title' => 'Про нас'])
<div style="margin: 0 auto; text-align: center">
    <div class="header_logo_mail">
        <a href="{{ route('pages.index') }}">
            <img style="width: 170px; height: 140px;" src="{{ asset('images/logo.png') }}">
        </a>
    </div>
    <div class="header_text_mail">
        <h1 style="font: 2.25em/2.375em 'PTSerif-Italic'; color: #0e3a40; white-space: nowrap">Вдалих вам покупок!</h1>
        <h2 style="font-size: 1.5em;color: black">З подякою "ТВОЯ НАДІЙНА АПТЕКА"</h2>
        <br>
        <a style="text-decoration: none; color:#0062cc" href="tel:+380639562389" class="desktop"><i class="icon-auricular-phone-symbol-in-a-circle"></i>&nbsp;&nbsp;+38 063 956 23 89</a>
        <a style="text-decoration: none; color:#0062cc; padding-left: 2em;" href="mailto:urpharmacy.com.ua@gmail.com">urpharmacy.com.ua@gmail.com</a>
    </div>
</div>