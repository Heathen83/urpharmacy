<div>
    <div class="row">
        <div class="col-lg-12">
            <div class="pull-right">
                <a class="btn btn-default" href="{{ route('orders.index') }}">
                    <span class="glyphicon glyphicon-arrow-left"></span> Назад
                </a>
                <a class="btn btn-default" href="{{ route('orders.edit', ['userId' => $order->id]) }}">
                    <span class="glyphicon glyphicon-edit"></span> Редагувати
                </a>
            </div>
        </div>
        <br>
        <br>
        <div class="row col-lg-10">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Інформація про замовлення:</h4></div>
                    <div class="panel-body">
                        <div class="row ">
                            <div class="col-lg-6">
                                <strong>Номер замовлення:</strong>
                            </div>
                            <div class="col-lg-6">{{ $order->id }}</div>
                        </div>
                        <div class="row ">
                            <div class="col-lg-6">
                                <strong>Ім'я</strong>
                            </div>
                            <div class="col-lg-6">{{ $order->name }}</div>
                        </div>
                        <div class="row ">
                            <div class="col-lg-6">
                                <strong>Номер телефону</strong>
                            </div>
                            <div class="col-lg-6">{{ $order->phone }}</div>
                        </div>
                        <div class="row ">
                            <div class="col-lg-6">
                                <strong>Коментар</strong>
                            </div>
                            <div class="col-lg-6">{{ $order->description }}</div>
                        </div>
                        <div class="row ">
                            <div class="col-lg-6">
                                <strong>Загальна сума замовлення</strong>
                            </div>
                            <div class="col-lg-6">{{ $order->total }} грн.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Товари:</h4></div>
                    @foreach($order->items as $item)
                    <div class="panel-body">
                        <div class="row ">
                            <div class="col-lg-6">
                                <strong>Код товару:</strong>
                            </div>
                            <div class="col-lg-6">{{ $item->drug->code }}</div>
                        </div>
                        <div class="row ">
                            <div class="col-lg-6">
                                <strong>Назва товару:</strong>
                            </div>
                            <div class="col-lg-6">
                                <a target="_blank" href="{{ route('product.show', $item->drug->drug->code) }}">
                                    {{ $item->drug->title }}
                                </a>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-lg-6">
                                <strong>Кількість:</strong>
                            </div>
                            <div class="col-lg-6">{{ $item->count }}</div>
                        </div>
                        <div class="row ">
                            <div class="col-lg-6">
                                <strong>Ціна за 1 одиницю</strong>
                            </div>
                            <div class="col-lg-6">{{ $item->amount }} грн.</div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .row-bordered:after {
        content: "";
        display: block;
        border-bottom: 1px solid #ccc;
        margin: 10px 15px;
    }
</style>