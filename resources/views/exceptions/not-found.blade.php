@include('layouts.header', ['title' => 'Сторінка не знайдена.'])
@include('layouts.nav')
<div class="content error">
    <div class="container">
        <div class="content__error__breadcrumb">
            <ul>
                <li><a href="{{ route('pages.index') }}">Головна</a></li>
                <li><span>&nbsp;/&nbsp;</span></li>
                <li><span>Помилка</span></li>
            </ul>
        </div>
        <div class="content__error">
            <ul>
                <li><p><img src="{{ asset('images/404.jpg') }}" alt=""></p>
                    <p>404<br><span>Сторінка, яку ви шукали, не існує.</span></p></li>
                <li><p>Ви можете повернутись&nbsp;<a href="{{ URL::previous() }}">назад</a>, на&nbsp;<a href="{{ route('pages.index') }}">головну
                            сторінку</a>,<br>скористатись пошуком, або меню в шапці сторінки.</p></li>
                <li><a href="{{ route('pages.index') }}">На головну</a></li>
            </ul>
        </div>
    </div>
</div>
@include('layouts.footer')