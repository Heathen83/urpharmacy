<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrugsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drugs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('code')->unique();
            $table->string('title');
            $table->string('int_title')->nullable();
            $table->string('maker')->nullable();
            $table->string('ats_code')->nullable();
            $table->mediumText('instruction')->nullable();
            $table->string('farm_ward')->nullable();
            $table->json('analogs')->nullable();
            $table->unsignedInteger('category_id')->nullable();
            $table->timestamps();
        });

        Schema::create('drug_portions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('drug_id');
            $table->string('code')->unique();
            $table->string('title');
            $table->string('description')->nullable();
            $table->double('price');
            $table->double('price_get');
            $table->integer('quantity');
            $table->unsignedInteger('type')->nullable();
            $table->timestamps();

            $table->foreign('drug_id')->references('id')->on('drugs')->onDelete('cascade');
        });

        Schema::create('drug_images', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('drug_id');
            $table->string('title');
            $table->string('path');

            $table->foreign('drug_id')->references('id')->on('drugs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drugs');
        Schema::dropIfExists('drug_portions');
        Schema::dropIfExists('drug_images');
    }
}
