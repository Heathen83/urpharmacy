<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnalogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analogs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('drug_id');
            $table->unsignedInteger('analog_id');
            $table->integer('type');
        });

        Schema::table('drugs', function (Blueprint $table) {
            $table->dropColumn('analogs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analogs');

        Schema::table('drugs', function (Blueprint $table) {
            $table->json('analogs')->nullable();
        });
    }
}
