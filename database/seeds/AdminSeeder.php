<?php

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Auth\Database\Role;
use Encore\Admin\Auth\Database\Permission;
use Encore\Admin\Auth\Database\Menu;

/**
 * Class AdminSeeder
 */
class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Administrator::create([
            'username' => 'admin',
            'password' => bcrypt('admin'),
            'name'     => 'Administrator',
        ]);

        Role::truncate();
        $administratorRole = Role::create([
            'name' => 'Адміністратор',
            'slug' => 'administrator',
        ]);

        // add role to user.
        Administrator::first()->roles()->save($administratorRole);

        //create a permission
        Permission::truncate();
        $allPerms = Permission::create([
            'name' => 'All permission',
            'slug' => '*',
            'http_method' => '',
            'http_path' => '*',
        ]);

        $dashboardPerm = Permission::create([
            'name' => 'Dashboard',
            'slug' => 'dashboard',
            'http_method' => 'GET',
            'http_path' => '/signups',
        ]);

        $loginPerms = Permission::create([
            'name' => 'Login',
            'slug' => 'auth.login',
            'http_method' => '',
            'http_path' => "/auth/login\r\nauth/logout",
        ]);

        $settingsPerms = Permission::create([
            'name' => 'User setting',
            'slug' => 'auth.setting',
            'http_method' => 'GET,PUT',
            'http_path' => '/auth/setting',
        ]);

        $managmentPerms = Permission::create([
            'name' => 'Admin management',
            'slug' => 'admin.management',
            'http_method' => '',
            'http_path' => "auth/roles\r\nauth/permissions\r\nauth/menu\r\nauth/logs",
        ]);

        $administratorRole->permissions()->save($allPerms);
        Menu::truncate();
        Menu::insert([
            [
                'parent_id' => 0,
                'order'     => 1,
                'title'     => 'Користувачі',
                'icon'      => 'fa-users',
                'uri'       => '/users',
            ],
            [
                'parent_id' => 0,
                'order'     => 2,
                'title'     => 'Ліки',
                'icon'      => 'fa-tint',
                'uri'       => '/drugs',
            ],
            [
                'parent_id' => 0,
                'order'     => 3,
                'title'     => 'Категорії',
                'icon'      => 'fa-bookmark-o',
                'uri'       => '/categories',
            ],
            [
                'parent_id' => 0,
                'order'     => 4,
                'title'     => 'Тип категорій',
                'icon'      => 'fa-bookmark',
                'uri'       => '/category_types',
            ]
        ]);
    }
}