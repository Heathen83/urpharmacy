<?php

use App\Models\Drug;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;

class DrugPortionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $drugs = Drug::all()->pluck('id')->toArray();

        foreach (range(1,40) as $index) {
            DB::table('drug_portions')->insert([
                'drug_id' => $faker->randomElement($drugs),
                'title' => $faker->company,
                'price' => $faker->randomFloat(2),
                'price_get' => $faker->randomFloat(2),
                'description' => $faker->realText(10),
                'quantity' => $faker->numberBetween(0, 20),
                'type' => $faker->numberBetween(1, 4)
            ]);
        }
    }
}
