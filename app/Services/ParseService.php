<?php

namespace App\Services;

use App\Models\DrugPortion;

class ParseService
{
    /**
     * @return bool|string
     */
    public function parseFromCSV()
    {
        $path = substr(__DIR__, 0, -21);
        $path .= '1C\articles.csv';

        if ($data = $this->csvToArray($path, ';')) {
            if ($message = $this->refreshGoods($data)) {
                return $message;
            }
        } else {
            return 'Файл для оновлення товарів не знайдено.';
        }

        return false;
    }

    /**
     * @param string $filename
     * @param string $delimiter
     * @return array|bool
     */
    private function csvToArray($filename='', $delimiter=',')
    {
        if (!file_exists($filename) || !is_readable($filename)) {
            return false;
        }

        $header = null;
        $data = [];

        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                $row = mb_convert_encoding($row, "utf-8", "windows-1251");

                if (!$header) {
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }

            fclose($handle);
        }

        return $data;
    }

    /**
     * @param $data
     * @return string
     */
    private function refreshGoods($data)
    {
        $statuses = [];

        foreach ($data as $item) {
            if (isset($item['КодТовара']) && $code = $item['КодТовара']) {
                if ($product = DrugPortion::whereCode($code)->first()) {
                    if (
                        $product->price != $item['ЦенаПрод'] ||
                        $product->price_get != $item['ЦенаПрих'] ||
                        $product->quantity != $item['Ост']
                    ) {
                        // Update if changes exists

                        $product->price = $item['ЦенаПрод'];
                        $product->price_get = $item['ЦенаПрих'];
                        $product->quantity = $item['Ост'];

                        if ($product->save()) {
                            $statuses[$code] = true;
                        }
                    }
                }
            }
        }

        return 'Оновлено товарів - ' . count($statuses);
    }
}