<?php

namespace App\Admin\Controllers;

use App\Models\Analog;
use App\Models\AtsCategory;
use App\Models\Category;
use App\Models\Post;
use App\Services\ParseService;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Models\User;
use App\Models\Drug;
use App\Models\DrugPortion;
use App\Models\DrugImages;
use Illuminate\Support\Str;

class PostController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Сторінки');

            $content->body($this->grid());
        });
    }

    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('Редагування сторінки');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('Редагування сторінки');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('Створення сторінки');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Post::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->title('Назва');
            $grid->slug('slug');

            $grid->filter(function ($filter) {
                $filter->like('title', 'Назва');
            });

            $grid->disableExport();

        });
    }

    /**
     * @param ParseService $parseService
     * @return bool|string
     */
    public function refresh(ParseService $parseService)
    {

    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Post::class, function (Form $form) {
            $form->tab('Загальна інформація', function (Form $form) {
                $form->display('id', '#');
                $form->text('title', 'Назва')->rules('required|string|max:255');
                $form->text('subtitle', 'Коротка інфо')->rules('nullable|string|max:255');
                $form->ckeditor('content', 'Вміст');
                $form->text('slug', 'slug');
            })
                ->tab('Медіa', function (Form $form) {
                    $form->file('image', 'Картинка')->rules('nullable');
                    $form->file('thumbnail', 'Мініатюра')->rules('nullable');
                });

            $form->tools(function (Form\Tools $tools) {
                $tools->disableView();
                $tools->disableList();
            });

            $form->disableEditingCheck();
            $form->disableViewCheck();
            $form->disableReset();
        });
    }

    /**
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store()
    {
        $this->prepareInput();
        return $this->form()->store();
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update($id)
    {
        $this->prepareInput();

        return $this->form($id)->update($id);
    }

    private function prepareInput()
    {
        if(is_null(request()->slug) || request()->slug=="") {
            $data['slug'] = Str::slug(request()->title);
            request()->merge($data);
        }

        if (request()->images) {
            foreach (request()->images as $key => $image) {
                if (isset($image['title'])) {
                    $data['images'][$key]['title'] = $image['title'];
                }

                if (isset($image['_remove_'])) {
                    $data['images'][$key]['_remove_'] = $image['_remove_'];
                }

                if (isset($image['path'])) {
                    $data['images'][$key]['path'] = $image['path'];
                }

                if (isset($image['id'])) {
                    $data['images'][$key]['id'] = $image['id'];
                }

                if (isset($image['file']) && $file = $image['file']) {
                    $filePath = 'media';
                    $fileName = uniqid() . '.' . $file->getClientOriginalExtension();

                    $file->storeAs('public/' . $filePath, $fileName);

                    $data['images'][$key]['path'] = 'storage/' . $filePath . '/' . $fileName;
                }
            }

            request()->merge($data);
        }
    }

}
