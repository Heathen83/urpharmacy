<?php

namespace App\Admin\Controllers;

use App\Models\Feedback;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Models\Category;

class FeedbackController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Звернення');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редагування звернень');
            $content->description('Редагування звернень');

            $content->body($this->form($id)->edit($id));
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редагування звернень');
            $content->description('Редагування звернень');

            $content->body($this->form($id)->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Створення звернень');
            $content->description('Створення нового звернення');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Feedback::class, function (Grid $grid) {

            $grid->name('Ім\'я')->sortable();
            $grid->phone('Телефон')->sortable();
            $grid->comment('Повідомлення');
            $grid->status('Статус')->display(function () {
                return Feedback::STATUSES[$this->status] ?? '';
            })->sortable();

            $grid->disableExport();

        });
    }

    /**
     * Make a form builder.
     *
     * @param null $id
     * @return Form
     */
    protected function form($id = null)
    {
        return Admin::form(Feedback::class, function (Form $form) use ($id) {
            $form->text('name', 'Ім\'я')->rules('required|string|min:4|max:191');
            $form->text('phone', 'Телефон')->rules('required|string|min:5|max:191');
            $form->text('comment', 'Повідомлення')->rules('required|string|min:5|max:666');
            $form->select('status', 'Статус')
                ->options(Feedback::STATUSES);

            $form->display('created_at', 'Створено о');
            $form->display('updated_at', 'Оновлено о');

            $form->tools(function (Form\Tools $tools) {
                $tools->disableView();
                $tools->disableList();
            });

            $form->disableEditingCheck();
            $form->disableViewCheck();
            $form->disableReset();
        });
    }

    /**
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store()
    {
        return $this->form()->store();
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update($id)
    {
        return $this->form($id)->update($id);
    }
}
