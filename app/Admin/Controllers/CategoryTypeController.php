<?php

namespace App\Admin\Controllers;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Models\CategoryType;

class CategoryTypeController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Тип категорій');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редагування типу');
            $content->description('Редагування типу');

            $content->body($this->form($id)->edit($id));
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редагування типу');
            $content->description('Редагування типу');

            $content->body($this->form($id)->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Створення типу категорії');
            $content->description('Створення нового типа категорії');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(CategoryType::class, function (Grid $grid) {
            $grid->name('Назва')->sortable();
            $grid->icon('Клас іконки');
            $grid->url('Силка');
            $grid->order('Порядок');
            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @param null $id
     * @return Form
     */
    protected function form($id = null)
    {
        return Admin::form(CategoryType::class, function (Form $form) use ($id) {
            $form->text('name', 'Назва')->rules('required|string|max:191');
            $form->text('icon', 'Клас іконки');
            $form->text('url', 'Силка');
            $form->number('order', 'Порядок')->default(0    );
            $form->image('image', 'Файл')->move('public/storage/images/');

            $form->tools(function (Form\Tools $tools) {
                $tools->disableView();
                $tools->disableList();
            });

            $form->disableEditingCheck();
            $form->disableViewCheck();
            $form->disableReset();
        });
    }

    /**
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store()
    {
        return $this->form()->store();
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update($id)
    {
        return $this->form($id)->update($id);
    }

}
