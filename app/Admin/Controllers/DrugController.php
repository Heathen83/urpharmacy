<?php

namespace App\Admin\Controllers;

use App\Models\Analog;
use App\Models\AtsCategory;
use App\Models\Category;
use App\Services\ParseService;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Models\Unit;
use App\Models\Drug;
use App\Models\DrugPortion;
use App\Models\DrugImages;

class DrugController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Ліки');

            $content->body($this->grid());
        });
    }

    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('Редагування ліків');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('Редагування ліків');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('Створення ліків');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Drug::class, function (Grid $grid) {

            $grid->id('ID')->sortable();

            $grid->code('Код товару')->sortable();
            $grid->title('Назва');
            $grid->maker('Виробник')->sortable();
            $grid->category_id('Категорія')->display(function () {
                return $this->category['title'] ?? '';
            })->sortable();

            $grid->filter(function ($filter) {
                $filter->like('code', 'Код');
                $filter->like('title', 'Назва');
                $filter->like('maker', 'Виробник');
            });

            $grid->tools(function ($tools) {
                $tools->append("<a href='/admin/goods/refresh' class='btn btn-sm btn-success'><i class=\"fa fa-arrow-circle-o-down\"></i></i> Оновити товари</a>");
            });

            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->prepend("<a href='/product/" .$actions->row->code . "' target='_blank'><i class=\"fa fa-eye\"></i></i></a>");
                $actions->disableView();
            });

            $grid->disableExport();

        });
    }

    /**
     * @param ParseService $parseService
     * @return bool|string
     */
    public function refresh(ParseService $parseService)
    {
        return $parseService->parseFromCSV();
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Drug::class, function (Form $form) {
            $form->tab('Загальна інформація', function (Form $form) {
                $form->display('id', '#');
                $form->text('code', 'Код')
                    ->rules(function ($form) {
                        return 'required|string|max:255|unique:drugs,code,' . $form->model()->id . ',id';
                    });
                $form->text('title', 'Назва')->rules('required|string|max:255');
                $form->text('int_title', 'Міжнародна назва')->rules('nullable|string|max:191');
                $form->text('ats_code', 'АТС код')->rules('nullable|string|max:191');
                $form->text('maker', 'Виробник')->rules('nullable|string|max:191');
                $form->textarea('instruction', 'Інструкція');
                $form->textarea('farm_ward', 'Фарм опіка')->rules('nullable');
                $form->text('youtube_link', 'Посилання на YouTube відео')->rules('nullable|string|max:255');
                $form->select('category_id', 'Категорія')
                    ->options(Category::pluck('title', 'id'));
                $form->select('ats_category_id', 'АТС Категорія')
                    ->options(AtsCategory::pluck('title', 'id'));
                $form->file('file', 'PDF Каталог');
                $form->ignore('file');
                $form->text('pdf_path', "")->placeholder("Виберіть файл каталога")->disable();
            })
                ->tab('Форми випуску', function (Form $form) {
                    $form->hasMany('portions', 'Форми випуску', function (Form\NestedForm $form) {
//                        $form->text('code', 'Код товару')
//                            ->rules(function ($form) {
//                                return 'required|string|max:255|unique:drug_portions,code,' . $form->model()->id . ',id';
//                            });
                        $form->text('title', 'Назва')->rules('required|string|max:255');
                        $form->textarea('description', 'Опис');
                        $form->decimal('price', 'Ціна')->rules('required|numeric');
                        $form->decimal('price_get', 'Ціна від виробника')->rules('required|numeric');
                        $form->decimal('discount_price', 'Ціна зі знижкою')->rules('nullable');
                        $form->number('quantity', 'Залишок')->rules('required|integer')->value(1);
                        $form->select('type', 'Тип')
                            ->options(Unit::getUnits())
                            ->rules('required');
                    });
                })
                ->tab('Аналоги', function (Form $form) {
                    $form->hasMany('analogs', 'Аналоги', function (Form\NestedForm $form) {
                        $form->select('analog_id', 'Аналог')
                            ->options(Drug::pluck('title', 'id'))
                            ->rules('required');
                        $form->select('type', 'Тип')
                            ->options(Analog::TYPES)
                            ->rules('required');
                    });
                })
                ->tab('Медіа', function (Form $form) {
                    $form->hasMany('images', 'Медіа', function (Form\NestedForm $form) {
                        $form->text('title', 'Назва')->rules('required|string|max:255');
                        $form->file('file', 'Файл');

                        $form->ignore('file');
                        $form->hidden('path');

                    });
                });

            $form->tools(function (Form\Tools $tools) {
                $tools->disableView();
                $tools->disableList();
            });

            $form->disableEditingCheck();
            $form->disableViewCheck();
            $form->disableReset();
        });
    }

    /**
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store()
    {
        $this->prepareInput();
        return $this->form()->store();
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update($id)
    {
        $this->prepareInput();

        return $this->form($id)->update($id);
    }

    private function prepareInput()
    {
        if (request()->file) {
            $file = request()->file;
            $filePath = 'pdf';
            $fileName = uniqid() . '.' . $file->getClientOriginalExtension();
            $file->storeAs('public/' . $filePath, $fileName);
            $data['pdf_path'] = 'storage/' . $filePath . '/' . $fileName;
            request()->merge($data);
        }
        if (request()->images) {
            foreach (request()->images as $key => $image) {
                if (isset($image['title'])) {
                    $data['images'][$key]['title'] = $image['title'];
                }

                if (isset($image['_remove_'])) {
                    $data['images'][$key]['_remove_'] = $image['_remove_'];
                }

                if (isset($image['path'])) {
                    $data['images'][$key]['path'] = $image['path'];
                }

                if (isset($image['id'])) {
                    $data['images'][$key]['id'] = $image['id'];
                }

                if (isset($image['file']) && $file = $image['file']) {
                    $filePath = 'media';
                    $fileName = uniqid() . '.' . $file->getClientOriginalExtension();

                    $file->storeAs('public/' . $filePath, $fileName);

                    $data['images'][$key]['path'] = 'storage/' . $filePath . '/' . $fileName;
                }
            }

            request()->merge($data);
        }

    }

}
