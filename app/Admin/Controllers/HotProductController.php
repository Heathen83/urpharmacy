<?php

namespace App\Admin\Controllers;

use App\Models\Drug;
use App\Models\HotProducts;
use App\Models\Order;
use App\Models\User;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Models\Category;

class HotProductController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Гарячі товари');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редагування товарів');
            $content->description('Редагування товарів');

            $content->body($this->form($id)->edit($id));
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редагування товарів');
            $content->description('Редагування товарів');

            $content->body($this->form($id)->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Створення списку товарів');
            $content->description('Створення нового списку товарів');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(HotProducts::class, function (Grid $grid) {
            $grid->drug_id('Продукт')->display(function () {
                return $this->drug['title'] ?? '';
            })->sortable();
            $grid->type('Тип')->display(function () {
                return HotProducts::TYPES[$this->type] ?? '';
            })->sortable();

            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @param null $id
     * @return Form
     */
    protected function form($id = null)
    {
        return Admin::form(HotProducts::class, function (Form $form) use ($id) {
            $form->select('drug_id', 'Продукт')
                ->options(Drug::all()->pluck('title', 'id'));
            $form->select('type', 'Тип')
                ->options(HotProducts::TYPES);

            $form->tools(function (Form\Tools $tools) {
                $tools->disableView();
                $tools->disableList();
            });

            $form->disableEditingCheck();
            $form->disableViewCheck();
            $form->disableReset();
        });
    }

    /**
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store()
    {
        return $this->form()->store();
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update($id)
    {
        return $this->form($id)->update($id);
    }
}
