<?php

namespace App\Admin\Controllers;

use App\Models\Order;
use App\Models\User;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Models\Category;
use Encore\Admin\Widgets\Box;

class OrderController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Замовлення');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $order = Order::with('items')->findOrFail($id);

            $box = new Box('Інформація про замовлення #' . $order->id, view('admin.orders')
                ->with(compact('order'))->render());
            $content->row($box);
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редагування замовлень');
            $content->description('Редагування замовлень');

            $content->body($this->form($id)->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Створення замовлення');
            $content->description('Створення нового замовлення');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Order::class, function (Grid $grid) {

            $grid->name('Ім\'я')->sortable();
            $grid->phone('Телефон')->sortable();
            $grid->description('Додаткова інформація')->sortable();
            $grid->user_id('Користувач')->display(function () {
                return $this->user['name'] ?? '';
            })->sortable();

            $grid->filter(function($filter) {
                $filter->like('name', 'Ім\'я');
                $filter->like('phone', 'Телефон');
                $filter->equal('user_id', 'ID Користувача');
            });

            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @param null $id
     * @return Form
     */
    protected function form($id = null)
    {
        return Admin::form(Order::class, function (Form $form) use ($id) {
            $form->text('name', 'Ім\'я')->rules('required|string|max:191');
            $form->text('phone', 'Телефон')->rules('required|string|max:191');
            $form->text('description', 'Додаткова інформація')->rules('required|string|max:191');
            $form->select('user_id', 'Користувач')
                ->options(User::all()->pluck('name', 'id'));

            $form->display('created_at', 'Створено о');
            $form->display('updated_at', 'Оновлено о');

            $form->tools(function (Form\Tools $tools) {
                $tools->disableView();
                $tools->disableList();
            });

            $form->disableEditingCheck();
            $form->disableViewCheck();
            $form->disableReset();
        });
    }

    /**
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store()
    {
        return $this->form()->store();
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update($id)
    {
        return $this->form($id)->update($id);
    }
}
