<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Repositories\CategoryRepository;
use App\Repositories\DrugRepository;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * @var CategoryRepository $category
     */
    protected $category;

    /**
     * @var DrugRepository $drugs
     */
    protected $drugs;

    public function __construct(CategoryRepository $category, DrugRepository $drugs)
    {
        $this->category = $category;
        $this->drugs = $drugs;
    }

    public function classificationShow($id = null, Request $request)
    {
        if ($id) {
            $category = $this->category->getAtsCategoryById($id);

            if ($category->subcategories) {
                $categories = $category->subcategories;
            } else {
                $categories[] = $category;
            }
        } else {
            $categories = $this->category->getAtsMainCategories();
        }

        $products = $this->category->getAtsProductsById($id);
        $products = $products->paginate(6);

        if(isset($request->load)){
            $html = view('layouts.list_product_elements')->with(compact('products'))->render();
            return response()->json(['success' => true, 'html' => $html]);
        }

        $actualProducts = $this->drugs->getActualProductsQuery()->take(8)->get();

        return view('pages.classification', compact('categories', 'actualProducts', 'products'));
    }
}
