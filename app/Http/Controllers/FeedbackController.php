<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FeedbackController extends Controller
{
    public function create(Request $request)
    {
        if (isset($request->nameFeed) || isset($request->telFeed)) {
            // Feedback form
            $rules = [
                'nameFeed' => 'required|string|min:2|max:90',
                'telFeed' => 'required|string',
                'commentFeed' => 'required|min:5|max:666'
            ];
            $request->validate($rules);

            $data['name'] = $request->nameFeed;
            $data['phone'] = $request->telFeed;
            $data['comment'] = $request->commentFeed;
        } elseif (isset($request->nameCall) || isset($request->telCall)) {
            // Call form
            $rules = [
                'nameCall' => 'required|string|min:2|max:90',
                'telCall' => 'required|string',
                'commentCall' => 'required|min:5|max:666',
                'timeFrom' => 'nullable|integer|min:0|max:24',
                'timeTo' => 'nullable|integer|min:0|max:24'
            ];
            $request->validate($rules);

            $data['name'] = $request->nameCall;
            $data['phone'] = $request->telCall;
            $data['comment'] = $request->commentCall;

            if (isset($request->timeFrom) && isset($request->timeTo)) {
                $data['comment'] = "[ЗАТЕЛЕФОНУВАТИ З $request->timeFrom ДО $request->timeTo]     ";
                $data['comment'] .= $request->commentCall;
            }
        } else {
            return false;
        }

        if (Feedback::create($data)) {
            $message = 'Дякуємо за Ваш коментар!';

            session()->flash('message', $message);

            return redirect(route('pages.index'));
        }

        return view('exceptions.not-found');
    }
}
