<?php

namespace App\Http\Controllers;

use App\Http\Requests\CartRequest;
use App\Models\DrugPortion;
use App\Repositories\DrugRepository;
use Gloudemans\Shoppingcart\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    /**
     * @var Cart
     */
    protected $cart;

    /**
     * @var DrugRepository
     */
    protected $drug;

    /**
     * CartController constructor.
     * @param Cart $cart
     * @param DrugRepository $drug
     */
    public function __construct(Cart $cart, DrugRepository $drug)
    {
        $this->cart = $cart;
        $this->drug = $drug;
    }

    /**
     * @param CartRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function add(CartRequest $request)
    {
        $drug = $this->drug->findPortion($request->code);
        $quantity = 1;

        if ($request->quantity) {
            $quantity = $request->quantity;
        }

        $quantitySum = (Session::has('quantity')) ? Session::get('quantity') : 0;
        $quantitySum += $quantity;
            Session::put('quantity', $quantitySum);

        $this->cart->add($drug->id, 'Portion'.$drug->id, $quantity, $drug->price);

        $message = 'Товар успішно доданий до корзини!';
        session()->flash('cartMessage', $message);

        return redirect()->back();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show() {
        $products = [];

        if ($cart = $this->cart->content()) {
            foreach ($cart as $item) {
                $drug = $this->drug->findPortion($item->id);
                $drug->rowId = $item->rowId;
                $drug->cartQuantity = $item->qty;

                $products[] = $drug;
            }
        }

        return view('pages.busket', [
            'products' => $products,
            'total' => $this->cart->total()
        ]);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function clear()
    {
        $this->cart->destroy();
        Session::put('quantity', 0);

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeQuantity(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'qty' => 'min:1|required|integer'
        ]);

        $this->cart->update($request->id, $request->qty);
        Session::put('quantity', $this->cart->count());

        return response()->json([
            'status' => true,
            'total' => $this->cart->total()
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);

        $this->cart->remove($request->id);
        Session::put('quantity', $this->cart->count());


        return response()->json([
            'status' => true,
            'total' => $this->cart->total()
        ]);
    }
}
