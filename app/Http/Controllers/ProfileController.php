<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * @var UserRepository $user
     */
    protected $user;

    /**
     * ProfileController constructor.
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user)
    {
        $this->user = $user;

        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        $user = Auth::user();

        return view('pages.personal_cabinet', compact('user'));
    }

    /**
     * @param Request $request
     * @return bool|\Illuminate\Http\RedirectResponse
     */
    public function changeGeneral(Request $request)
    {
        $rules = [
            'tel' => 'required|string',
            'firstName' => 'required|string|min:4|max:45'
        ];
        $request->validate($rules);

        $data['phone'] = $request->tel;
        $data['name'] = $request->firstName;

        if ($this->user->update(Auth::user(), $data)) {
            return redirect()->back();
        }

        return false;
    }

    public function changeEmail(Request $request)
    {
        $rules = [
            'email' => 'required|email'
        ];
        $request->validate($rules);

        $data['email'] = $request->email;

        if ($this->user->update(Auth::user(), $data)) {
            return redirect()->back();
        }

        return false;
    }

    public function changePassword(Request $request)
    {
        $rules = [
            'newPassword' => 'required|string|min:6',
        ];
        $request->validate($rules);

        $data['password'] = bcrypt($request->newPassword);

        if ($this->user->update(Auth::user(), $data)) {
            return redirect()->back();
        }

        return false;
    }
}
