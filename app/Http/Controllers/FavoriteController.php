<?php

namespace App\Http\Controllers;

use App\Models\DrugPortion;
use App\Models\Favorite;
use App\Models\Feedback;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavoriteController extends Controller
{
    /**
     * FavoriteController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add(Request $request)
    {
        $rules = [
            'code' => 'required|integer'
        ];
        $request->validate($rules);

        $code = $request->code;
        $user = Auth::user();

        if ($code && $user) {
            $drug = DrugPortion::findOrFail($code);

            $favorite = Favorite::firstOrNew([
               'user_id' => $user->id,
               'drug_portion_id' => $drug->id
            ]);

            if ($favorite->save()) {
                return redirect(route('cabinet.show'));
            }
        }

        return view('exceptions.not-found');
    }

    public function remove(Request $request)
    {
        $rules = [
            'code' => 'required|integer'
        ];
        $request->validate($rules);

        $code = $request->code;
        $user = Auth::user();

        if ($code && $user) {
            $drug = DrugPortion::findOrFail($code);

            $user->favorites()
                ->where('drug_portion_id', $drug->id)
                ->delete();

            return redirect()->back();
        }

        return view('exceptions.not-found');
    }
}
