<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * App\Models\DrugImages
 *
 * @property int $id
 * @property int $drug_id
 * @property string $title
 * @property string $path
 * @property-read \App\Models\Drug $drug
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DrugImages whereDrugId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DrugImages whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DrugImages wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DrugImages whereTitle($value)
 * @mixin \Eloquent
 */
class DrugImages extends Model
{
    protected $fillable = [
        'drug_id',
        'title',
        'path'
    ];

    public $timestamps = false;

    public static function boot() {
        parent::boot();

        static::deleting(function($image) {
            Storage::delete('public' . substr($image->path, 7));
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function drug()
    {
        return $this->belongsTo(Drug::class);
    }
}
