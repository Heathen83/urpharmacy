<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\OrderItems
 *
 * @property int $id
 * @property int $drug_portion_id
 * @property int $order_id
 * @property int $count
 * @property-read \App\Models\DrugPortion $drug
 * @property-read \App\Models\Order $order
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderItems whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderItems whereDrugPortionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderItems whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderItems whereOrderId($value)
 * @mixin \Eloquent
 */
class OrderItems extends Model
{
    protected $fillable = [
        'drug_portion_id',
        'order_id',
        'count',
        'amount'
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function drug()
    {
        return $this->belongsTo(DrugPortion::class, 'drug_portion_id', 'id');
    }
}
