<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Cart
 *
 * @property int $id
 * @property int|null $user_id
 * @property int $drug_portion_id
 * @property int $count
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\DrugPortion $drug
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserCart whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserCart whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserCart whereDrugPortionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserCart whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserCart whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserCart whereUserId($value)
 * @mixin \Eloquent
 */
class UserCart extends Model
{
    protected $fillable = [
        'user_id',
        'drug_portion_id',
        'count',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function drug()
    {
        return $this->belongsTo(DrugPortion::class, 'id', 'drug_portion_id');
    }
}
