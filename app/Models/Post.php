<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';
    protected $primaryKey='id';

    const INFO_CATEGORY  = 1;

    protected $fillable = [
        'title', 'parent_id', 'order', 'image', 'content', 'subtitle', 'slug', 'category_id', 'thumbnail'
    ];

    public function parent()
    {
        return Post::findOrFail($this->parent_id);
    }

    public function childs()
    {
        return Post::where('parent_id',$this->id)->get();
    }

    public function pictures()
    {
        return $this->hasMany(PostPicture::class);
    }

    public static function getPostsToDropDown($category_id = 1, $post_id=null){
        if($post_id==null) {
            $posts = Post::where('category_id', $category_id)
                ->get()
                ->pluck('title', 'id');
        }
        else{
            $posts = Post::where('category_id', $category_id)
                ->where('id','<>',$post_id)
                ->get()
                ->pluck('title', 'id');
        }
        $posts[""] = 'Не має';
        return $posts;
    }

    public function getThumbnail(){
        return is_null($this->thumbnail) ? $this->image : $this->thumbnail;
    }
}
