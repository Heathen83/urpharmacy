<?php

namespace App\Repositories;

use App\Models\User;

/**
 * Class UserRepository
 *
 * @package App\Repositories
 */
class UserRepository
{
    /**
     * @param array $data
     *
     * @return User|false
     */
    public function create(array $data)
    {
        $user = new User($data);

        return $user->save() ? $user : false;
    }

    /**
     * @param User $user
     * @param array $data
     *
     * @return bool
     */
    public function update(User $user, array $data)
    {
        return $user->update($data);
    }

    /**
     * @param User $user
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function delete(User $user)
    {
        \DB::beginTransaction();

        try {

            if ($user->delete()) {

                \DB::commit();

                return true;
            }

        } catch (\Exception $e) {
            report($e);
        }

        \DB::rollBack();

        return false;
    }

    /**
     * @param array $filters
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getAll(array $filters = [])
    {
        return User::filter($filters)->paginate();
    }

    /**
     * @param User $user
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getAllOrders(User $user)
    {
        return $user->orders()->get();
    }
}
