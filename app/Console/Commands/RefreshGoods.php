<?php

namespace App\Console\Commands;

use App\Services\ParseService;
use Illuminate\Console\Command;

class RefreshGoods extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'goods:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Оновити товари';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $parseService = new ParseService();

        return $this->info($parseService->parseFromCSV());
    }
}
